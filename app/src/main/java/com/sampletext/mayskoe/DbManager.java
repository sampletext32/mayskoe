package com.sampletext.mayskoe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbManager extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;//версия БД
    public static final String DATABASE_NAME = "internalDb";//название БД
    //названия таблиц
    public static final String TABLE_DATA_NAME = "data";
    //названия полей таблицы Data
    public static String KEY_DATA_ID = "id";
    public static String KEY_DATA_CONTENT = "content";

    public DbManager(Context context, String name,
                     SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    //полупаттерн Singleton
    public static DbManager getInstance(Context context) {
        return new DbManager(context, null, null, DATABASE_VERSION);
    }

    public String getById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_DATA_NAME, null, KEY_DATA_ID + " = ?", new String[]{Integer.toString(id)}, null, null, null);
        String data = "";
        if (cursor.moveToFirst()) {
            data = cursor.getString(1);
        }
        cursor.close();
        db.close();
        return data;
    }

    public void replace(int id, String content) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATA_CONTENT, content);
        db.update(TABLE_DATA_NAME, values, KEY_DATA_ID + "=?", new String[]{Integer.toString(id)});
    }

    //обработчик создания
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TABLE_DATA_NAME +
                        " (" + KEY_DATA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_DATA_CONTENT + " TEXT)"
        );
        for (int i = 0; i < 60; i++) {
            ContentValues values = new ContentValues();
            values.put(KEY_DATA_CONTENT, "");
            int id = (int) db.insert(TABLE_DATA_NAME, null, values);
            int a = 5;
        }
    }

    //при обновление базы
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //удаляем все таблицы
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA_NAME);

        onCreate(db);//создаём таблицы заново
    }

    //обработчик отката базы
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);//делаем то же что и при обновлении
    }
}
