package com.sampletext.mayskoe;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DataActivity extends AppCompatActivity {

    TextView textViewHeader;
    EditText editTextContent;

    int _id;
    String _content;

    View.OnClickListener buttonSaveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    TextWatcher editTextContentTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            _content = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DbManager.getInstance(DataActivity.this).replace(_id, _content);
        Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
        finish();//при нажатии на кнопку назад
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        textViewHeader = findViewById(R.id.textViewHeader);
        editTextContent = findViewById(R.id.editTextContent);
        editTextContent.addTextChangedListener(editTextContentTextWatcher);

        //включить кнопку назад в заголовке
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        int id = getIntent().getIntExtra("id", -1);
        if (id != -1) {
            String content = DbManager.getInstance(this).getById(id);
            _content = content;
            _id = id;
            editTextContent.setText(_content);
            textViewHeader.setText(String.format("Открыт %d", _id));
        } else {
            Toast.makeText(getApplicationContext(), "Error Passing Id", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
