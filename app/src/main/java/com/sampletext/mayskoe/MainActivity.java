package com.sampletext.mayskoe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public void onClick(View v){
        int id = Integer.parseInt(v.getTag().toString());
        Intent intent = new Intent(getApplicationContext(), DataActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //deleteDatabase(DbManager.DATABASE_NAME);
    }
}
